Marion Technical College has over 40 years of experience in providing training and continuing education to adults throughout Marion County. As the Adult Education Division of the Marion County Public Schools, MTC serves over 3,000 students and participants annually.

Address: 1014 SW 7th Rd, Ocala, FL 34471

Phone: 352-671-7200
